//client side code - will only execute inside browser
(async function () {

    const elTodoList = document.getElementById('todo-list');
    const elTodoCount = document.getElementById('todo-count');

    try {
        const response = await fetch('/api/v1/todos').then(r => r.json());
        const { results: todos, count } = response
        elTodoList.innerHTML = todos.map(todo => `
            <li>${todo.title}</li>
        `).join('')
        elTodoCount.innerText = `Todos: ${count}`;
    } catch (e) {
        console.log(e.message);
    }

})();