//Setting up a node server 

//require express from node modules
const express = require('express');
const readLine = require('readline');
const os = require('os');
const http = require('http');
const packageJson = require('./package.json').name
//Initialise express as app
const app = express();


//Define a port with default value of 3000
const { PORT = 5000 } = process.env;

const totalMemory = `Total Memory: ${os.totalmem()}`;
const freeMemory = `Free Memory: ${os.freemem()}`;
const cpuData = `CPU Cores: ${os.cpus().length}`;
const architecture = `Architecture: ${os.arch()}`;
const platform = `Platform: ${os.platform()}`;
const releaseInfo = `Release: ${os.release()}`;
const userInfo = `User Info: ${os.userInfo().username}`;
const displayOsInfo = `\n${totalMemory}\n${freeMemory}\n${cpuData}\n${architecture}\n${platform}\n${releaseInfo}\n${userInfo}`;

const fs = require('fs');
const rl = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
});


rl.question(`
Choose an option: 
1. Read package .json
2. Display OS info
3. Start HTTP Server
Type a number: `, function (option) {
    switch (option) {
        case '1':
            const data = fs.readFileSync('package.json', 'utf8');
            console.log(data);
            break;
        case '2':
            console.log(`${displayOsInfo}`);
            break
        case '3':
            console.log(`${server}`);
            break
    }
    rl.close();
});


//listening on traffic at port 3000

const serverStart = app.listen(PORT, () => {
    console.log(`Server started on ${PORT}...`);
});

/*

const server = http.createServer((req, res) => {
    res.write('Starting HTTP Server..')
});

server.listen(5000);
*/
